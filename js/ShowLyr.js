(function($) {
	$.fn.ShowLyr = function(options) {
		var option = $.extend({}, $.fn.ShowLyr.setting, options);
		var parent = $(this);
		var lyrs = [];
		var objLyr = {
			hei: -1,
			cur: 0,
			set: function(time, pause) {
				var node = null;
				var last = 0;
				time += option.aheadTime;
				for (var i = 0; i < lyrs.length - 1; i++) {
					node = lyrs[i];
					// 找出现在要播放，但是没有播放的歌词
					if (node.time < time && time < lyrs[i + 1].time && !node.line.attr('style')) {
						// 要是这歌词不是第一段歌词，就移除上一段歌词的现实
						if (objLyr.cur != 0 || i != 0)
							lyrs[objLyr.cur].line.removeAttr('style');
						// 这段歌词是中间插入的(就是时间不是从开头开始)
						if (time - node.time > option.resetTime) {
							node.line.width(node.style.width * (time - node.time) /
								parseInt(node.style.transitionDuration));
							// 要是是停止状态下的就不放动画
							if (!pause) {
								setTimeout(function() {
									node.line.css({
										'transitionDuration': lyrs[i + 1].time - time + 's',
										'width': node.style.width
									});
								}, 100);
							}
						} else if (!pause) {
							node.line.css(node.style);
						}
						last = i;
						objLyr.cur = i;
						parent.css({
							'transform': 'translateY(' + (-last * objLyr.hei) + 'px)'
						});
						break;
					}
				};
			},
			switc: function(pauseFlag, time) {
				time += option.aheadTime;
				if (pauseFlag)
					objLyr.pause(time);
				else
					objLyr.play(time);
			},
			pause: function(time) {
				var node = lyrs[objLyr.cur];
				var width = node.line.width();
				node.line.width(width);
			},
			play: function(time) {
				var index = objLyr.cur;
				var node = lyrs[index];
				node.line.width(node.style.width);
				node.line.css({
					'transitionDuration': lyrs[index + 1].time - time + 's',
					'width': node.style.width
				});
			}
		};

		getLyr(function(data, status) {
			var arr = [];
			if (status)
				arr = data.split('\n');
			for (var i = 0; i < arr.length; i++) {
				createLine(lyrs, arr[i]);
			};
			if (option.ready) {
				option.ready();
			}
			init();
		});

		function getLyr(callback) {
			$.ajax({
				url: option.url.replace('mp3','lrc'),
				type: 'GET',
				success: function(data) {
					callback(data, true);
				},
				error: function(data) {
					console.log('Fail to get lyrics !');
					console.log(data);
				}
			});
		}

		function init() {
			parent.html('');
			parent.css({
				'transform': 'translateY(0px)'
			});
			var node = null;
			var html = '';
			var patt = '<div class="' + option.line + '"><div class="gray">{con}</div>' +
				'<div class="highlight">{con}</div></div><br>';
			for (var i = 0; i < lyrs.length - 1; i++) {
				html = patt.replace(/{con}/g, lyrs[i].str);
				node = $(html);
				parent.append(node);
				// 配置对象
				lyrs[i].line = node.children('.highlight');
				lyrs[i].style = {
					'transitionDuration': lyrs[i + 1].time - lyrs[i].time + 's',
					'width': node.children('.gray').width()
				};
			};

			node = parent.children('.line');
			objLyr.hei = parseInt(node.css('marginTop')) * 2 + node.height();
		}

		return objLyr;
	}

	function createLine(arr, str) {
		var start = str.indexOf('[');
		var end = str.indexOf(']');
		if (start < 0 || end < 0)
			return null;
		var time = transTimeToNum(str.substring(start + 1, end));
		var str = str.substring(end + 1);
		var obj = {
			'time': time,
			'str': str
		}
		arr.push(obj);
		// console.log(obj);
	}

	function transTimeToNum(str) {
		var tim = str.split(':');
		var min = parseInt(tim[0], 10);
		var sec = parseFloat(tim[1], 10);
		return min * 60 + sec;
	}

	$.fn.ShowLyr.setting = {
		url: '/res/1.lrc',
		line: 'line',
		resetTime: 0.5,
		aheadTime: 0.6,
		ready: function() {}
	}
})(jQuery);