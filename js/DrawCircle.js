(function($) {
	$.fn.DrawCircle = function(options) {
		var option = $.extend({}, $.fn.DrawCircle.setting, options);
		var fir = $('.' + option.right + '> div', this);
		var sec = $('.' + option.left + '> div', this);
		var cen = $('.' + option.center, this);
		var circle = {
			now: 0,
			flag: true,
			set: function(num) {
				var per = Number(num);

				if (isNaN(per) || per > 100 || per < 0)
					return;

				if(circle.flag) {
					circle.flag = false;
					if (circle.now > per)
						circle.moveToStart(per, updata);
					else
						circle.moveToEnd(per, updata);
				} else {
					// console.log('You click too frequent!');
				}

				function updata() {
					if (circle.now > 50 && per < 50)
						sec.css('transform', 'rotate(0deg)');
					else if (circle.now < 50 && per > 50)
						fir.css('transform', 'rotate(180deg)');
					circle.now = per;
					circle.flag = true;
				}
			},
			moveToEnd: function(per, callback) {
				var ang1 = per * 3.6;
				var ang2 = per * 3.6;
				var count = circle.now * 3.6;
				ang1 = ang1 > 180 ? 180 : ang1;
				ang2 = ang2 < 180 ? 0 : ang2;
				fun1();

				function fun1() {
					var timer = setInterval(function() {
						if (count <= ang1) {
							fir.css('transform', 'rotate(' + count + 'deg)');
						} else {
							fir.css('transform', 'rotate(' + ang1 + 'deg)');
							clearTimeout(timer);
							fun2();
						}
						count += 2;
					}, 10);
				}

				function fun2() {
					var timer = setInterval(function() {
						if (count <= ang2) {
							sec.css('transform', 'rotate(' + (count - 180) + 'deg)');
						} else {
							if (callback)
								callback();
							clearTimeout(timer);
						}
						count += 2;
					},10);
				}

			},
			moveToStart: function(per, callback) {
				var ang1 = per * 3.6;
				var ang2 = per * 3.6;
				var count = circle.now * 3.6;
				ang1 = ang1 > 180 ? 180 : ang1;
				ang2 = ang2 < 180 ? 180 : ang2;
				fun1();

				function fun1() {
					var timer = setInterval(function() {
						if (count >= ang2) {
							sec.css('transform', 'rotate(' + (count - 180) + 'deg)');
						} else {
							sec.css('transform', 'rotate(' + (ang2 - 180) + 'deg)');
							clearTimeout(timer);
							fun2();
						}
						count -= 2;
					}, 10);
				}

				function fun2() {
					var timer = setInterval(function() {
						if (count >= ang1 && count <= 180) {
							fir.css('transform', 'rotate(' + count + 'deg)');
						} else {
							if (callback)
								callback();
							clearTimeout(timer);
						}
						count -= 2;
					},10);
				}

			},
			setCallback: function(callback) {
				if(option.callback)
					circle.callback = option.callback;
				if(callback)
					circle.callback = callback;
				return circle;
			}
		};

		// 计算比例部分
		var CountRate = {
			width: 0,
			height: 0,
			pi: 3.1415926 * 2,
			clickRate: function(event) {
				var p = {x:0,y:0};
				var x = event.offsetX;
				var y = event.offsetY;
				p.x = CountRate.height - y
				p.y = CountRate.width - x;
				var tan = Math.atan2(p.y, p.x);
				tan = tan > 0 ? CountRate.pi - tan : Math.abs(tan);
				tan = tan / CountRate.pi * 100;
				// console.log(tan);
				// 接近0的4%都默认为0
				if(tan < 1 || tan > 98)
					tan = 0;
				circle.set(tan);
				if(circle.callback)
					circle.callback(tan);
			},
			init: function() {
				cen.bind('click',this.clickRate);
				this.width = cen.width() / 2;
				this.height = cen.height() / 2;
			}
		}

		CountRate.init();

		return circle.setCallback();
	}

	$.fn.DrawCircle.setting = {
		left: 'player-cir-lef',
		right: 'player-cir-rig',
		center: 'player-cir-eventcover',
		callback: null
	}
})(jQuery);

// var a = $('#player-cir').DrawCircle();
// setInterval(function() {
// 	var ran = Math.floor(Math.random() * 100);
// 	console.log('--------------');
// 	console.log(ran);
// 	a.set(ran);
// }, 10000);


// a.set(89);
// setTimeout(function() {
// 	a.set(60);
// }, 4000);