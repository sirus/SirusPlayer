(function($) {
	$.fn.SirusPlayer = function(options) {
		var option = $.extend({}, $.fn.SirusPlayer.setting, options);
		var songList = ['/res/1.mp3', '/res/2.mp3', '/res/3.mp3'];
		var curIndex = 0;
		return this.each(function() {
			var audio = this;
			var playBtn = $('#' + option.playBtn);
			var nextBtn = $('#' + option.nextBtn);
			var prevBtn = $('#' + option.prevBtn);
			var circle = $('#' + option.circBtn).DrawCircle();
			var lyric = $('#' + option.lyriBtn).ShowLyr({'url':songList[curIndex]});
			var soInfo = $('#' + option.circBtn + ' .fir');
			// 播放+暂停
			playBtn.bind('click', function() {
				if (!audio.paused) {
					playBtn.addClass(option.playBtn_play);
					playBtn.removeClass(option.playBtn_pause);
					audio.pause();
				} else {
					playBtn.addClass(option.playBtn_pause);
					playBtn.removeClass(option.playBtn_play);
					audio.play();
				}
				lyric.switc(audio.paused, audio.currentTime);
			});
			// 歌曲切换
			nextBtn.bind('click', function() {
				var flag = true;
				if(audio.paused)
					flag = false;
				curIndex = (curIndex + 1) % 3;
				lyric = $('#' + option.lyriBtn).ShowLyr({'url':songList[curIndex]});
				audio.src = songList[curIndex];
				circle.set(0);
				if(flag)
					audio.play();
			});
			prevBtn.bind('click', function() {
				curIndex = (curIndex - 1) % 3;
				lyric = $('#' + option.lyriBtn).ShowLyr({'url':songList[curIndex]});
				audio.src = songList[curIndex];
				circle.set(0);
				if(!audio.paused)
					audio.play();
			});
			// 初始化歌曲信息
			audio.onloadedmetadata = function() {
				var time = getTimeFromSecond(audio.duration);
				$('#' + option.circBtn + ' .las').html(time);
				circle.setCallback(function(percen) {
					audio.currentTime = percen * audio.duration / 100;
				});
			};
			// 播放进度
			audio.ontimeupdate = function() {
				var time = getTimeFromSecond(audio.currentTime);
				var rate = audio.currentTime / audio.duration * 100;
				$('#' + option.circBtn + ' .fir').html(time);
				circle.set(rate);
				lyric.set(audio.currentTime, audio.paused);
			}
		});
	}

	function getTimeFromSecond(duration) {
		if (isNaN(duration))
			return 0;
		var min = Math.floor(duration / 60);
		var sec = Math.floor(duration % 60);
		return min + ':' + formatNum(sec, 2);
	}

	function formatNum(num, tar) {
		if (isNaN(num))
			return num;
		var abs = num < 0 ? '-' : '';
		var len = Math.abs(num).toString().length + 1;
		var zer = '';
		for (var i = tar - len; i >= 0; i--) {
			zer += '0';
		};
		return abs + zer + Math.abs(num);
	}

	$.fn.SirusPlayer.setting = {
		playBtn: 'player-play',
		circBtn: 'player-cir',
		lyriBtn: 'player-lyr',
		nextBtn: 'player-nex',
		prevBtn: 'player-pre',
		resourcePath: '/res/',
		playBtn_play: 'icon-play',
		playBtn_pause: 'icon-pause',
	}
})(jQuery);

$('#player').SirusPlayer();